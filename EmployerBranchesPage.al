page 50100 EmployerBranches
{
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = EmployerBranches;

    layout
    {
        area(Content)
        {
            repeater(GeneralTab)
            {
                field(LineNo; LineNo)
                {
                    ApplicationArea = All;
                }
                field(EmployerCode; EmployerCode)
                {
                    ApplicationArea = All;
                }
                field(BranchCode; BranchCode)
                {
                    ApplicationArea = All;
                }
                field(BranchName; BranchName)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}

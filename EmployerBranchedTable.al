table 50100 EmployerBranches
{
    Caption = 'Employer Branches';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; LineNo; Integer)
        {
            DataClassification = ToBeClassified;
            MinValue = 1;
            Caption = 'Line No';
            AutoIncrement = true;
        }
        field(2; EmployerCode; code[20])
        {
            TableRelation = Customer."No.";
            Caption = 'Employer Code';
        }
        field(3; BranchCode; Code[30])
        {
            Caption = 'Branch Code';
        }
        field(4; BranchName; Text[50])
        {
            Caption = 'Branch Name';
        }
        field(5; DateCreated; DateTime)
        {
            Caption = 'Timestamp';
        }
    }

    keys
    {
        key(PK; LineNo)
        {
            Clustered = true;
        }
        key(CK; EmployerCode)
        {
        }
    }

}
